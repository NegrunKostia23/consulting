if (document.getElementById('map')) {
  var googleMap = document.querySelector('#map'),
    googleMapData = googleMap.dataset;

  var mapCenter = googleMapData.center.split(','),
      mapMarker = googleMapData.coordinates.split(',');


  function initialize() {
    var map = new google.maps.Map(
      document.getElementById('map'), {
        center: new google.maps.LatLng(parseFloat(mapCenter[0]), parseFloat(mapCenter[1])),
        zoom: parseInt(googleMapData.zoom),
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f7f2ed"},{"lightness":20}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#cbf2f2"}]}]
      });

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(parseFloat(mapMarker[0]), parseFloat(mapMarker[1])),
      map: map,
      icon: {
        url: "img/placeholder-filled-point.svg",
        scaledSize: new google.maps.Size(64, 64)
      }
    });
  }

  function loadMap() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.setAttribute('defer','');
    script.setAttribute('async','');
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3' +
      '&key=' + googleMapData.key +'&callback=initialize'; //& neededP
    document.body.appendChild(script);
  }

  window.onload = loadMap;
}


