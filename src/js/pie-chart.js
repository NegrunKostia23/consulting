

window.chartColors = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
};


window.randomScalingFactor = function() {
  return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
};

var piChart = $("#piChart");
var lineChart = $("#lineChart");

if (piChart.length) {
  var myChart = new Chart(piChart, {
    type: 'pie',
    data: {
      labels: ["Option 1", "Option 2", "Option 3"],
      datasets: [{
        data: [12, 19, 3],
        backgroundColor: [
          'rgba(18, 73, 117, 1)',
          'rgba(23, 199, 193, 1)',
          'rgb(77, 166, 223)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      responsive: true,
      legend: {
        labels: {
          fontColor: '#41617a',
          fontSize: 13
        },
        position: 'bottom'
      }
    }
  });
}


if (lineChart.length) {
  var lineChart = new Chart(lineChart, {
    type: 'line',
    data: {
      labels: ["1", "2", "3", "4", "5"],
      datasets: [{
        label: "Attribute 1",
        backgroundColor: "#124975",
        data: [
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor()
        ],
        fill: true
      }, {
        label: "Attribute 1",
        backgroundColor: "#4da6df",
        data: [
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor()
        ],
        fill: true
      }]
    },
    options: {
      legend: {
        position: 'bottom'
      },
      responsive: true,
      tooltips: {
        mode: 'index',
        intersect: false
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: false,
            labelString: 'Month'
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: false,
            labelString: 'Value'
          }
        }]
      }
    }
  });
}