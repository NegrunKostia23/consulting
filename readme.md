# Consulting
### Must have on your computer:
1. [Git](https://git-scm.com/download) (Guaranteed to work on: 2.10.1 Windows)
2. [Node.js](https://nodejs.org/en/download) LTS (Guaranteed to work on: 6.9.1 Windows)

-----------------------------------------------

### Packages:
For global dependencies needed on computer:
```
npm install browser-sync gulp -g
```
For install local dependencies:
```
npm install
```

-----------------------------------------------

If you changed some dependencies, or updated it, - please, manually update package.json
If have issues after update, Use:
```
npm cache clean
```

-----------------------------------------------

### All compiling with Gulp!!!
For build(included clean and default task), just type:
```
gulp
```
For local server, or local development(included clean and dev task), just type:
```
gulp serve
```

-----------------------------------------------

### Structure:
* "src" folder - project sources
* "dev" folder - local dev folder (local server work from it, not stored in git)
* "dist" folder - deploy version (not stored in git)
* "temp" folder - there could be other files (not stored in git)

```
+-- dist
|   +-- fonts
|   |   --- anyfile.eot
|   |   --- anyfile.svg
|   |   --- anyfile.ttf
|   |   --- anyfile.woff
|   +-- img
|   |   --- anyfile.jpg
|   |   --- anyfile.png
|   |   --- anyfile.svg
|   +-- js
|   |   +-- vendor
|   |   |   --- jquery.js
|   |   |   --- ***.js
|   |   |   --- ***.js
|   |   --- main.js
|   +-- css
|   |   --- main.css
|   --- index.html
+-- dev
|   +-- fonts
|   |   --- anyfile.eot
|   |   --- anyfile.svg
|   |   --- anyfile.ttf
|   |   --- anyfile.woff
|   +-- img
|   |   --- anyfile.jpg
|   |   --- anyfile.png
|   |   --- anyfile.svg
|   +-- js
|   |   +-- vendor
|   |   |   --- jquery***.js
|   |   --- main.js
|   +-- css
|   |   --- main.css
|   --- index.html
+-- src
|   +-- fonts
|   |   --- anyfile.eot
|   |   --- anyfile.svg
|   |   --- anyfile.ttf
|   |   --- anyfile.woff
|   +-- img
|   |   --- anyfile.jpg
|   |   --- anyfile.png
|   |   --- anyfile.svg
|   +-- inc
|   |   --- header.html
|   |   --- footer.html
|   +-- js
|   |   +-- vendor
|   |   |   --- jquery.js
|   |   |   --- ***.js
|   |   |   --- ***.js
|   |   --- main.js
|   +-- scss
|   |   --- _general.scss
|   |   --- main.scss
|   --- index.html
+-- temp
|   --- anyfile.psd
|   --- anyfile.zip
--- .gitignore
--- gulpfile.js
--- package.json
--- readme.md
```